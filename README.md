# net-Security-homework

#### 介绍
研究生信息安全课程大作业，使用NSL-KDD数据集训练一个网络入侵检测模型，并用KDDCup和NSL-KDD数据集进行模型评估。

#### 使用环境

python == 3.7.9

scikit-learn == 0.19.1

numpy == 1.15.4

pandas == 1.1.2

#### 文件介绍

./data/ : 程序使用到的NSL-KDD网络入侵检测数据集和KDD-CUP网络入侵检测数据集

./model/ : 训练完成的基于SVM的网络入侵检测模型以及PCA降维模型

model_with_pca.ipynb: 训练利用PCA降维后的数据的网络入侵检测模型

model_no_pca.ipynb: 训练未使用PCA降维的数据的网络入侵检测模型

get_KDD_cup_data.ipynb: 处理KDD-CUP数据集

read_kddcup99.py: 将KDD-CUP数据集从特殊文件读如到.csv文件中

evaluate_model_with_kdddataset.ipynb: 使用KDD-CUP数据集对训练好的网络入侵检测数据集进行评估

#### 训练完成的模型

IDS_model-8-0.m: 使用PCA降维后的数据训练的网络入侵检测模型

NO_PCA_IDS_model.m: 使用未降维数据训练的网络入侵检测模型

pca_model.m: PCA降维模型